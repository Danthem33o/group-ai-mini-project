﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/// <summary>
/// Minimax algorithm that determines the best move for the player to make
/// by checking all possible outcomes.
/// </summary>
public class MiniMax {

	/// <summary>
	/// Initializes a new instance of the <see cref="MiniMax"/> class.
	/// </summary>
	/// <param name="game">Game is the current game instance.</param>
	public MiniMax(Game game) {
		performAIMove (game);
	}

	/// <summary>
	/// Performs the AI move.
	/// </summary>
	/// <param name="game">Game is the current game instance.</param>
	private void performAIMove(Game game) {			
		Node bestMove = miniMax (game, true); 				 //   Find the best move
		game.makeMove(false, bestMove.getX(), bestMove.getY());  //   Make the move
	}

	/// <summary>
	/// Performs the minimax algorithm. 
	/// 
	/// Node value of a win = 10.
	/// Node value of a draw = -1.
	/// Node value of a lose = -10.
	/// </summary>
	/// <returns>The best node value.</returns>
	/// <param name="game">Game is the current game instance.</param>
	/// <param name="maxP">If set to <c>true</c> it is max player's turn</param>
	private Node miniMax(Game game, bool maxP) {
		
		//	Base case, end if...
		game.hasWon();						//   Sets the game variables

		if (game.gameOver) {
			if (game.aiWon) {
				game.resetGameVars ();		//	Reset the game vars			
				return new Node (10);   //	after finding outcome
			} else if (game.userWon) {
				game.resetGameVars ();
				return new Node (-10);
			} else if (game.draw) {
				game.resetGameVars ();
				return new Node (-1);
			}
		} 

		//   Array of all root moves
		ArrayList moves = new ArrayList();
		for (int x = 0; x < 3; x++)			//   Cycle through current avalable moves
			for (int y = 0; y < 3; y++)
				if (game.isSpaceAt (x, y)) {
					Node node = new Node(x, y);

					if (maxP) {
						game.makeMove (false, x, y); 
						node.setScore (miniMax (game, false).getScore ());
					} else {  //   Player turn
						game.makeMove(true, x, y);
						node.setScore (miniMax (game, true).getScore ());
					}

					moves.Add (node);	
					game.undoMove (x, y); //    Remove the move when done
				}

		//	Find the best move for the current player 
		Node bestMove = new Node();
		if (maxP) {
			int bestScore = int.MinValue + 1;
			foreach (Node move in moves) {
				if (move.getScore() > bestScore) {
					bestMove = move;
					bestScore = move.getScore ();
				}
			}
		} else {
			int bestScore = int.MaxValue - 1;
			foreach (Node move in moves) {
				if (move.getScore() < bestScore) {
					bestMove = move;
					bestScore = move.getScore ();

				}
			}
		}
		return bestMove; //  Return the best move for current player
	}

} //   End class
