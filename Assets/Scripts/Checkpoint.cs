﻿using UnityEngine;
using System.Collections;

public class Checkpoint : MonoBehaviour {

    private GameController platformer;

	// Use this for initialization
	void Start () {
        platformer = FindObjectOfType<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter2D(Collider2D character){

        if (character.name == "Player"){
            platformer.currentCheckpoint = gameObject;
        }

    }
}
