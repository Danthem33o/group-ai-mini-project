﻿using UnityEngine;
using System.Collections;

public class DestroyOnLeave : MonoBehaviour {

	//Destroys any object that leaves the game area. Used to keep track of projectile attacks etc as garbage collection.
	void OnTriggerExit2D(Collider2D other){
		Destroy (other.gameObject);
	}
}
