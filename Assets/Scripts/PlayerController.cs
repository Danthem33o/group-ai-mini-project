﻿using UnityEngine;
using System.Collections;


public class PlayerController : MonoBehaviour {

    //movement variables
    public float maxSpeed;
	bool facingright = true;

    //jumping variables
    public float jump;
    bool doubleJump;
	public bool grounded = false;
	public Transform groundCheck;
	float groundRadius = 0.2f;
	//This layer mask is used to check if any objects below the player are on the ground layer i.e are ground. 
	public LayerMask groundLayer;

    //ladder climbing variables
    public bool ladderClimb;
    public float climbSpeed;
    private float climbVelocity;
    private float gravityStore;

	//stealth and crouching variables
	bool crouching = false;

    //character live and health
    public int lives = 3;
    public int health = 10;

	//Physics variables
	Rigidbody2D rb;

    //Colectibles
    public bool haveKey;

    //Animator 
    Animator animator;

    //game cotroller
    private GameController gameController;

    //Tic Tac Toe minigame related
    public bool onTTTArea;


    //Runs at game startup.
    void Start () {
		rb = this.GetComponent<Rigidbody2D>();
		animator = this.GetComponent<Animator>();
        gameController = FindObjectOfType<GameController>();
        gravityStore = rb.gravityScale;

		
    }
	
	// Update is called once per frame
	void FixedUpdate  () {

		//only update player character if in battlemode
		if (GameController.battlemode=="MainGame") {

			//check for ground below the player
			grounded = Physics2D.OverlapCircle (groundCheck.position, groundRadius, groundLayer);
            if (grounded)
                doubleJump = false;

			//get input from the horizontal axis
			float move = Input.GetAxis ("Horizontal");

			//pass required variables to animator
			animator.SetFloat ("speed", Mathf.Abs (move));
			animator.SetBool ("grounded", grounded);
			animator.SetFloat ("verticalSpeed", rb.velocity.y);
			animator.SetBool ("crouching", crouching);

			//move player by adding force to the players ridgidbody physics componentas
			rb.velocity = new Vector2 (move * maxSpeed, rb.velocity.y);

			//Check for change in direction
			if (move > 0 && !facingright) {
				Flip ();
			} else if (move < 0 && facingright) {
				Flip ();
			}

		}
	}

    void Update() {

        //only update player character if in battlemode
        if (GameController.battlemode == "MainGame") {

            if (ladderClimb)
            {
                rb.gravityScale = 0f;
                climbVelocity = climbSpeed * Input.GetAxisRaw("Vertical");
                rb.velocity = new Vector2(rb.velocity.x, climbVelocity);
            }


            if (!ladderClimb)
            {
                rb.gravityScale = gravityStore;
            }

            //Jump function
            if (grounded && Input.GetKeyDown(KeyCode.Space)) {
                Jump();
            }

            //double jump function
            if (!doubleJump && !grounded && Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
                doubleJump = true;
            }
            //Crouch function, checks for the C key
            if (Input.GetKeyDown(KeyCode.C)|| Input.GetKeyUp(KeyCode.C)) {
				crouching = !crouching;
			}

            if(Input.GetKeyDown(KeyCode.E)&& onTTTArea)
            {
                gameController.ActivateTTT();

            }




        }
	}

    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jump);
        grounded = false;
    }


	//flips the transform 180* for animations depending on direction
	void Flip(){
		facingright = !facingright;
		//gets the scale of the transform
		Vector3 scale = transform.localScale;
		//changes all x values to their negative counterpart
		scale.x *= -1;
		//asigns scale to game
		transform.localScale = scale;
	}

	


}
