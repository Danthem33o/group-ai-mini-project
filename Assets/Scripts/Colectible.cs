﻿using UnityEngine;
using System.Collections;

public class Colectible : MonoBehaviour
{

    private PlayerController playerController;

    public string type;

    // Use this for initialization
    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D character)
    {

        if (character.name == "Player")
        {
            if (type == "Key")
                playerController.haveKey = true;
            if (type == "Health")
            {
                if (playerController.health < 50)
                    playerController.health += 50;
                else
                    playerController.health = 100;
            }
            gameObject.SetActive(false);

        }
    }
}
