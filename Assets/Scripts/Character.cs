﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {

    public int health;
    public int damageAdj;


    public void setHealth(float damage)
    {
        float _damage = (float)((float)damageAdj / 100.0) * (float)damage + (float)damage;
        health = health - (int)_damage;
    }

    public void SetArgs(int _health, int _damageAdj)
    {
        health = _health;
        damageAdj = _damageAdj;
    }
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
