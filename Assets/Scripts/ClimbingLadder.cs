﻿using UnityEngine;
using System.Collections;

public class ClimbingLadder : MonoBehaviour {

    private PlayerController player;

	// Use this for initialization
	void Start () {
        player = FindObjectOfType<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter2D(Collider2D character)
    {
        if (character.name == "Player")
            player.ladderClimb = true;
    }

    void OnTriggerExit2D(Collider2D character)
    {
        if (character.name == "Player")
            player.ladderClimb = false;
    }
}
