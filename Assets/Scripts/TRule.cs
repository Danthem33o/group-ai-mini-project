﻿using UnityEngine;
using System.Collections;

public class TRule : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

        public bool matched;
        public int weight;
        public string antecedentA;
        public string antecedentB;
        public string antecedentC;

        public TRule()
        {
            matched = false;
            weight = 0;
        }

        public void setRule(string A, string B, string C)
        {
            antecedentA = A;
            antecedentB = B;
            antecedentC = C;
        }
}
