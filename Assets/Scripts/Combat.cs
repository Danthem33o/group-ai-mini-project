﻿using UnityEngine;
using System.Collections;
using System;

public class Combat : MonoBehaviour {

        public static int combat = 0;
        public static int successfulStrike = 0;
        public static int successfulDefence = 0;

        public static string processStrikeA(Character attacker, string attack, Character defendant, string defence)
        {

            string status = "";
            if (string.Equals(attack, "Kick", StringComparison.OrdinalIgnoreCase) && 
            !string.Equals(defence, "Block",System.StringComparison.OrdinalIgnoreCase))
            {
                status = processDamage(attacker, defendant, 3);
            }
            else if (string.Equals(attack, "Sword", StringComparison.OrdinalIgnoreCase) &&
            !string.Equals(defence, "Parry", System.StringComparison.OrdinalIgnoreCase))
            {
                status = processDamage(attacker, defendant, 6);
            }
            else if (string.Equals(attack, "Star", StringComparison.OrdinalIgnoreCase) &&
            !string.Equals(defence, "Dodge", System.StringComparison.OrdinalIgnoreCase))
            {
                status = processDamage(attacker, defendant, 8);
            }
            else
            {
                combat++;

                if ((combat == 2 && successfulDefence == 1) || (combat == 3 && successfulDefence == 1))
                {
                int ran = UnityEngine.Random.Range(1, 100);
                Console.WriteLine(ran);
                    if (ran<20)
                    {
                        attacker.setHealth(6);
                        status = "Retaliate!";
                    }
                }
                else if (successfulDefence == 2)
                {

                    if (UnityEngine.Random.Range(1,100) < 50)
                    {
                        attacker.setHealth(9);
                        status = "Retaliate!";
                    }
                }
            successfulDefence++;
            }
            if (combat == 3)
            {
                successfulDefence = 0;
                successfulStrike = 0;
                combat = 0;
            }

            return status;
        }

        public static string processDamage(Character attacker, Character defendant, float damage)
        {
            string status = "";
            combat++;
            if ((combat == 2 && successfulStrike == 1) || (combat == 3 && successfulStrike == 1))
            {
                if (UnityEngine.Random.Range(1,100) < 20)
                {
                    damage = (float)damage + (float)((float)damage * (50.0 / 100.0));
                    defendant.setHealth(damage);
                    status = "Critical 1.5X!";
                }
                else
                {
                    defendant.setHealth(damage);
                }
            }
            else if (successfulStrike == 2)
            {
                if (UnityEngine.Random.Range(1, 100) < 50)
                {
                    damage = damage * 2;
                    defendant.setHealth(damage);
                    status = "Critical 2X!";
                }
                else
                {
                    defendant.setHealth(damage);
                }
            }
            else
        {
            defendant.setHealth(damage);
        }
            successfulStrike++;
            if (combat == 3)
            {
                successfulDefence = 0;
                successfulStrike = 0;
                combat = 0;
            }
            return status;
        }

}
