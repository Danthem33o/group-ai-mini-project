﻿using UnityEngine;
using System.Collections;

public class MinigameDoor : MonoBehaviour
{

    private PlayerController player;
    public GameObject doorstop;


    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D character)
    {
        if (character.name == "Player")
        {
            GameController.battlemode = "TTT";

        }
    }
}
