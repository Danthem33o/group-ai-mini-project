using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GuardScript : MonoBehaviour 
{
	EnemyEyes eyes;
	EnemyVisualisation visionCone;
	float speed = -3;
	bool facingright;
	public bool provokesturnbased, facingleft;
	TrainedNetwork guardbrain;
    public GameController gameController;
	public int bravery;
	public int patrolsize, escapetime;
	int guardstate;
	double distancetotarget;
	Transform lastseenat;
	Vector3 startposition, lastturnposition;
	public GameObject player;
	SpriteRenderer sprite;
	Animator unitAnimator;
    public int health;
    public int enemyid;
	public Text guardReaction;

    //shurkien attack variables
    public Transform firepoint;
    public GameObject shuriken;
    float attackInterval;

	
	void Start() 
	{
		//initialise enemies eyes
		eyes = GetComponentInChildren<EnemyEyes>();
		visionCone = GetComponentInChildren<EnemyVisualisation>();
		sprite = GetComponentInChildren<SpriteRenderer>();
		unitAnimator = GetComponentInChildren<Animator>();

		startposition = this.transform.position;
		lastturnposition = this.transform.position;

		//enemy bravery random between 1-75;
		if(bravery == 0)
			bravery = (int)Random.Range(1, 75);


        gameController = FindObjectOfType<GameController>();

		//set guard to patrol
		guardstate = 1;
		distancetotarget = 8;

		//used to start the guard facing the right direction
		if (!facingleft && patrolsize == 0) {
			Flip ();
		}



	}
	
	void FixedUpdate()
	{
		//check game is in platformer mode
        if (GameController.battlemode == "MainGame")
        {
            if (attackInterval > 0)
                attackInterval -= Time.deltaTime;
            //guards patrol state
            if (guardstate == 1)
            {
                if (transform.position.x < startposition.x - patrolsize || transform.position.x > startposition.x + patrolsize)
                {
					//make sure the guard is supposed to be moving.
                    if (patrolsize != 0)
                    {
                        speed *= -1;
                        Flip();
                    }
                }

                //moves the unit if they have a patrol.
                if (patrolsize != 0)
                {
                    transform.position = new Vector3(transform.position.x + speed * Time.fixedDeltaTime, transform.position.y, transform.position.z);
                }

				//set guards vision to idle.
                visionCone.status = EnemyVisualisation.Status.Idle;

            }

            //spotted state
            if (guardstate == 2)
            {
				//sets vision to yellow.
                visionCone.status = EnemyVisualisation.Status.Suspicious;
                Invoke("playerSpotted", escapetime);

                //code cut from final - used to allow the AI to move slowly towards the players last know position
                /*if (transform.position.x < lastseenat.position.x) {
                    transform.position = new Vector3 (transform.position.x + 2 * Time.fixedDeltaTime, transform.position.y, transform.position.z);
                }

                if (transform.position.x > lastseenat.position.x) {
                    transform.position = new Vector3 (transform.position.x - 2 * Time.fixedDeltaTime, transform.position.y, transform.position.z);
                }*/


            }

            //alert state
            if (guardstate == 3)
            {
                visionCone.status = EnemyVisualisation.Status.Alert;

				//checks to see if the guard is supposed to attack the player in turn based combat or real time
                if (provokesturnbased)
                {
                    gameController.ActivateTurnBased2(enemyid, health, this.gameObject);
                    guardstate = 1;

                }
                else
                {
                    if (attackInterval <= 0)
                    {
                        Instantiate(shuriken, firepoint.position, firepoint.rotation);
                        attackInterval = (float)0.7;
                    }
                }


            }

            //pretend not to see player
            if (guardstate == 4)
            {
                if (transform.position.x < startposition.x - patrolsize || transform.position.x > startposition.x + patrolsize)
                {

                    if (patrolsize != 0)
                    {
                        speed *= -1;
                        Flip();
                    }
                }

                //moves the unit if they have a patrol
                if (patrolsize != 0)
                {
                    transform.position = new Vector3(transform.position.x + speed * Time.fixedDeltaTime, transform.position.y, transform.position.z);
                }
                visionCone.status = EnemyVisualisation.Status.Idle;

				//takes guard out of patrol cycle
            }

            //surrender state!
            if (guardstate == 5)
            {	
				//rotate view
				DisplayMessage("I GIVE UP!!");
                visionCone.status = EnemyVisualisation.Status.Idle;
				eyes.enabled = false;
				visionCone.enabled = false;
                this.transform.eulerAngles = new Vector3(180, 180, 0);
				sprite.transform.eulerAngles = new Vector3(0, 0, 0);

				//takes guard out of patrol cycle
				guardstate = 6;
            }
        }
	}
	
	void Update()
	{
        //Debug.Log(GameController.battlemode);
        if (GameController.battlemode == "MainGame")
        {
            //Debug.Log("Update gs: " + guardstate);

            bool playerInView = false;

            //look for the player if they are not visible.
            if (guardstate == 1)
            {
                playerInView = checkForPlayer();
            }

            if (playerInView)
            {
                //set guard to noticed
                guardstate = 2;

            }

            //determines if the unit has moved and changes between idle and moving animations.
            if (lastturnposition != this.transform.position)
            {
                unitAnimator.SetInteger("Speed", (int)speed);
            }
            else
            {
                unitAnimator.SetInteger("Speed", 0);
            }

            lastturnposition = this.transform.position;
        }

	}

	/** check to see if player is in view. */
	bool checkForPlayer(){
		foreach (RaycastHit2D hit in eyes.hits) {
			if ( hit.transform!=null && hit.transform.tag == "Player") {
				lastseenat = player.transform;
				distancetotarget = hit.distance;
				return true;
			} 
		}

		return false;
	}

	/** when the player is spotted, call this after 2 seconds to determine action. */
	void playerSpotted(){
        if (GameController.battlemode == "MainGame")
        {
            if (checkForPlayer())
            {
                decideAction();
            }
            else
            {
                guardstate = 1;
            }
        }
	}

	/**
	 * When the player is sighted, this is used to decide the units course of action.
	 * */
	void decideAction(){
		//playerhealth 1-100, playerkills 0-25, difficulty 1-3, enemybravery 1-75, distance 1-8, attack 0-1, fake 0-1, surrender 0-1;
		double playerhealth = (double)player.GetComponent<PlayerController>().health;
	
		double playerkills = (double)GameController.kills;

		double difficulty = (double)GameController.difficulty;

		visionCone.status = EnemyVisualisation.Status.Suspicious;

		//
		int action = GuardBrains.guardbrain.getAction (playerhealth, playerkills, difficulty, bravery, distancetotarget);

		//attack
		if (action == 0) {
			visionCone.status = EnemyVisualisation.Status.Alert;
			guardstate = 3;
		}

		//pretend
		if (action == 1) {
			visionCone.status = EnemyVisualisation.Status.Idle;
			guardstate = 4;

			DisplayMessage("I umm.... didn't see anything, OK?");
		}

		//surrender
		if (action == 2) {
			visionCone.status = EnemyVisualisation.Status.Idle;
			guardstate = 5;
		}
	}

	/**
	 * Function used to flip the sprite and guard eye visualisation.
	 * */
	void Flip(){
		facingright = !facingright;
		//gets the scale of the transform
		this.transform.Rotate(0, 180, 0);
		sprite.flipY = !sprite.flipY;
	}

	void DisplayMessage(string guardspeak){
		if (guardReaction != null) {
			guardReaction.text = guardspeak;
			guardReaction.transform.position = Camera.main.WorldToScreenPoint (this.transform.position) + new Vector3 (0, 5, 0);
			guardReaction.enabled = true;
			Invoke ("HideText", 2);
		}
	}

	void HideText(){
		guardReaction.enabled = false;
	}
}
