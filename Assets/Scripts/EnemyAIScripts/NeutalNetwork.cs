﻿using UnityEngine;
using System.Collections;

public class TrainedNetwork{
	//playerhealth 1-100, playerkills 0-25, difficulty 1-3, enemybravery 1-75, distance 1-8, attack 0-1, fake 0-1, surrender 0-1;
	static double[][] trainingset = new double[][]{
		new double[]{78,8,3,21,6,0.6,0.7,0.7},//
		new double[]{47,16,3,54,7,0.9,0.3,0.5},//
		new double[]{58,0,2,48,2,0.6,0.2,0.3},//
		new double[]{87,15,2,35,6,0.5,0.6,0.4},//
		new double[]{42,0,1,8,3,0.3,0.6,0.9},//
		new double[]{100,4,1,8,8,0.3,0.9,0.9},//
		new double[]{49,21,2,50,4,0.5,0.4,0.4},//
		new double[]{8,10,3,69,4,0.9,0.2,0.2},//
		new double[]{4,14,1,35,1,0.7,0.1,0.4},//
		new double[]{27,14,3,27,4,0.3,0.6,0.6},//
		new double[]{91,1,3,13,4,0.3,0.8,0.8},//
		new double[]{66,14,3,56,8,0.8,0.3,0.1},//
		new double[]{41,9,2,66,3,0.7,0.2,0.3},//
		new double[]{33,5,2,8,6,0.3,0.8,0.9},//
		new double[]{22,20,2,25,1,0.5,0.5,0.7},//
		new double[]{69,0,1,71,1,0.9,0.1,0.2},//
		new double[]{36,20,2,64,8,0.9,0.3,0.2},//
		new double[]{39,11,1,52,2,0.5,0.2,0.4},//
		new double[]{23,23,2,69,6,0.8,0.2,0.1},//
		new double[]{33,20,1,39,3,0.5,0.2,0.3},//
		new double[]{42,6,3,26,3,0.5,0.3,0.4},//
		new double[]{77,22,1,63,5,0.8,0.2,0.3},//
		new double[]{30,4,2,43,2,0.5,0.1,0.3},//
		new double[]{21,8,1,38,3,0.5,0.2,0.3},//
		new double[]{51,18,1,37,6,0.5,0.4,0.3},//
		new double[]{45,5,1,30,5,0.4,0.4,0.2},//
		new double[]{6,10,3,9,1,0.3,0.3,0.8},//
		new double[]{37,2,3,50,6,0.7,0.3,0.2},//
		new double[]{25,10,3,13,6,0.5,0.4,0.3},//
		new double[]{68,23,1,9,2,0.3,0.6,0.7},//
		new double[]{86,14,3,65,8,0.9,0.2,0.1},//
		new double[]{51,11,2,55,6,0.7,0.3,0.2},//
		new double[]{71,9,3,72,1,0.9,0.1,0.2},//
		new double[]{59,25,3,6,3,0.4,0.4,0.5},//
		new double[]{22,6,1,29,8,0.5,0.6,0.4},//
		new double[]{40,6,3,67,4,0.9,0.2,0.2},//
		new double[]{7,11,1,42,7,0.6,0.4,0.3},//
		new double[]{77,9,1,10,8,0.3,0.7,0.6},//
		new double[]{44,4,1,10,3,0.4,0.5,0.6},//
		new double[]{16,2,1,65,1,0.8,0.2,0.4},//
		new double[]{86,11,1,5,1,0.1,0.2,0.8},//
		new double[]{21,11,2,39,5,0.6,0.2,0.4},//
		new double[]{70,12,2,69,1,0.9,0.1,0.2},//
		new double[]{52,17,3,63,7,0.8,0.3,0.2},//
		new double[]{49,15,1,29,8,0.4,0.4,0.4},//
		new double[]{99,23,1,65,2,0.8,0.2,0.1},//
		new double[]{94,9,1,12,4,0.2,0.4,0.4},//
		new double[]{41,10,3,52,1,0.7,0.1,0.3},//
		new double[]{80,19,2,15,2,0.2,0.5,0.7},//
		new double[]{52,13,3,41,6,0.6,0.4,0.3},//
		new double[]{78,6,2,6,6,0.1,0.7,0.4},//
		new double[]{31,8,1,51,8,0.6,0.3,0.1},//
		new double[]{73,17,1,8,8,0.1,0.9,0.8},//
		new double[]{37,3,3,2,3,0.2,0.5,0.8},//
		new double[]{76,12,2,39,3,0.5,0.4,0.3},//
		new double[]{78,12,3,35,8,0.5,0.4,0.2},//
		new double[]{47,22,3,51,2,0.6,0.3,0.4},//
		new double[]{9,7,1,8,7,0.1,0.7,0.8},//
		new double[]{43,20,1,62,2,0.9,0.2,0.3},//
		new double[]{21,5,2,55,8,0.8,0.4,0.2},//
		new double[]{77,25,1,32,1,0.5,0.2,0.7},//
		new double[]{74,12,1,18,1,0.2,0.5,0.6},//
		new double[]{82,15,2,56,6,0.7,0.4,0.3},//
		new double[]{100,11,1,46,8,0.7,0.5,0.2},//
		new double[]{43,15,2,69,1,0.9,0.2,0.4},//
		new double[]{52,11,2,67,7,0.9,0.3,0.2},//
		new double[]{88,7,3,47,7,0.6,0.3,0.2},//
		new double[]{58,6,3,62,8,0.7,0.3,0.2},//
		new double[]{83,13,3,50,1,0.8,0.2,0.4},//
		new double[]{99,20,3,26,8,0.4,0.5,0.4},//
		new double[]{10,15,1,69,7,0.9,0.2,0.1},//
		new double[]{57,7,2,16,2,0.4,0.8,0.9},//
		new double[]{70,10,2,43,5,0.6,0.3,0.2},//
		new double[]{54,25,2,66,7,0.8,0.4,0.3},//
		new double[]{13,5,1,7,1,0.1,0.7,0.9},//
		new double[]{17,4,1,67,2,0.9,0.3,0.3},//
		new double[]{10,1,1,48,1,0.6,0.2,0.3},//
		new double[]{48,14,2,29,3,0.4,0.35,0.4},//
		new double[]{15,18,3,12,4,0.3,0.5,0.5},//
		new double[]{39,6,1,18,4,0.3,0.4,0.4},//
		new double[]{33,20,1,39,3,0.5,0.2,0.3},//
		new double[]{42,6,3,26,3,0.5,0.4,0.3},//
		new double[]{77,22,1,63,5,0.8,0.2,0.3},//
		new double[]{30,4,2,43,2,0.5,0.3,0.3},//
		new double[]{21,8,1,38,3,0.5,0.2,0.3},//
		new double[]{51,18,1,37,6,0.5,0.4,0.3},//
		new double[]{45,5,1,30,5,0.4,0.4,0.2},//
		new double[]{6,10,3,9,1,0.3,0.3,0.8},//
		new double[]{37,2,3,50,6,0.7,0.3,0.2},//
		new double[]{25,10,3,13,6,0.5,0.4,0.3},//
	};

	NeuralNetwork brain = new NeuralNetwork();

	public TrainedNetwork(){
		brain.Initialize(5, 3, 3);
		brain.SetLearningRate(0.15);
		brain.SetMomentum(true, 0.9);
		trainBrain();
	}

	//playerhealth 1-100, playerkills 0-25, difficulty 1-3, enemyhealth 1-75, distance 1-8
	public int getAction(double playerhealth, double playerkills, double difficulty, double enemybravery,  double distance){
		brain.SetInput(0, playerhealth);
		brain.SetInput(1, playerkills);
		brain.SetInput(2, difficulty);
		brain.SetInput(3, enemybravery);
		brain.SetInput(4, distance);

		brain.FeedForward();

		double max = -1000.0;
		int index = -1000;
		for(int j=0; j<3; j++)
		{
			if (max < brain.GetOutput(j))
			{ 
				max = brain.GetOutput(j);
				index = j;
			}		
		}
		return index;
	}
	
	public int trainBrain(){
		double	error = 1;
		int		c = 0;
		
		while((error > 0.04) && (c<1000))
		{
			error = 0;
			c++;
			for(int i=0; i<84; i++)
			{
				brain.SetInput(0, trainingset[i][0]);
				brain.SetInput(1, trainingset[i][1]);
				brain.SetInput(2, trainingset[i][2]);
				brain.SetInput(3, trainingset[i][3]);
				brain.SetInput(4, trainingset[i][4]);
				
				
				brain.SetDesiredOutput(0, trainingset[i][5]);
				brain.SetDesiredOutput(1, trainingset[i][6]);
				brain.SetDesiredOutput(2, trainingset[i][7]);
				
				
				brain.FeedForward();
				error += brain.CalculateError();
				brain.BackPropagate();
				
			}

			error = error / 84.0f;
		}

		return c;
	}
}




//------Neural Network Class-------------------//
public class NeuralNetwork{
	NeuralNetworkLayer	inputLayer = new NeuralNetworkLayer();
	NeuralNetworkLayer	hiddenLayer = new NeuralNetworkLayer();
	NeuralNetworkLayer	outputLayer = new NeuralNetworkLayer();

	public NeuralNetwork(){
		/*inputLayer = new NeuralNetworkLayer();
		hiddenLayer = new NeuralNetworkLayer();
		outputLayer = new NeuralNetworkLayer();*/
	}

	public void Initialize(int numinputnodes, int numhiddennodes, int numoutputnodes)
	{
		inputLayer.nodes = numinputnodes;
		inputLayer.childnodes = numhiddennodes;
		inputLayer.parentnodes = 0;
		inputLayer.Initialize(numinputnodes, null, hiddenLayer);
		inputLayer.RandomizeWeights();
		
		hiddenLayer.nodes = numhiddennodes;
		hiddenLayer.childnodes = numoutputnodes;
		hiddenLayer.parentnodes = numinputnodes;
		hiddenLayer.Initialize(numhiddennodes, inputLayer, outputLayer);
		hiddenLayer.RandomizeWeights();
		
		outputLayer.nodes = numoutputnodes;
		outputLayer.childnodes = 0;
		outputLayer.parentnodes = numhiddennodes;
		outputLayer.Initialize(numoutputnodes, hiddenLayer, null);
	}

	public void	SetInput(int i, double value)
	{
		if((i>=0) && (i<inputLayer.nodes))
		{
			inputLayer.neuronvalues[i] = value;
		}
	}

	public double GetOutput(int i)
	{
		if((i>=0) && (i<outputLayer.nodes))
		{
			return outputLayer.neuronvalues[i];
		}
		
		return 10000f; // to indicate an error
	}

	public void SetDesiredOutput(int i, double value)
	{
		if((i>=0) && (i<outputLayer.nodes))
		{
			outputLayer.desiredvalues[i] = value;
		}
	}

	public void FeedForward()
	{
		inputLayer.CalculateNeuronValues();
		hiddenLayer.CalculateNeuronValues();
		outputLayer.CalculateNeuronValues();
	}

	public void BackPropagate()
	{
		outputLayer.CalculateErrors();
		hiddenLayer.CalculateErrors();
		
		hiddenLayer.AdjustWeights();
		inputLayer.AdjustWeights();
	}

	public int	GetMaxOutputID()
	{
		int		id;
		double	maxval;
		
		maxval = outputLayer.neuronvalues[0];
		id = 0;
		
		for(int i=1; i<outputLayer.nodes; i++)
		{
			if(outputLayer.neuronvalues[i] > maxval)
			{
				maxval = outputLayer.neuronvalues[i];
				id = i;
			}
		}
		
		return id;
	}

	public double CalculateError()
	{
		int		i;
		double	error = 0;
		
		for(i=0; i<outputLayer.nodes; i++)
		{
			double x = outputLayer.neuronvalues[i] - outputLayer.desiredvalues[i];
			error += x * x;
		}
		
		error = error / outputLayer.nodes;
		
		return error;
	}

	public void SetLearningRate(double rate)
	{
		inputLayer.learningrate = rate;
		hiddenLayer.learningrate = rate;
		outputLayer.learningrate = rate;
	}
	
	public void	SetLinearOutput(bool useLinear)
	{
		inputLayer.linearoutput = useLinear;
		hiddenLayer.linearoutput = useLinear;
		outputLayer.linearoutput = useLinear;
	}

	public void	SetMomentum(bool useMomentum, double factor)
	{
		inputLayer.usemomentum = useMomentum;
		hiddenLayer.usemomentum  = useMomentum;
		outputLayer.usemomentum  = useMomentum;
		
		inputLayer.momentumfactor = factor;
		hiddenLayer.momentumfactor = factor;
		outputLayer.momentumfactor = factor;
		
	}

}
//------Neural Network Class END-------------------//

//------Neural Layer Class-------------------//
public class NeuralNetworkLayer{
	public int nodes, parentnodes, childnodes;
	public double[][] weights;
	public double[][] weightchanges;
	public double[] neuronvalues, desiredvalues, errors, biasweights, biasvalues;
	public double learningrate;

	public bool usemomentum, linearoutput;
	public double momentumfactor;

	NeuralNetworkLayer parentLayer, childLayer;

	public NeuralNetworkLayer(){
		parentLayer = childLayer = null;
		linearoutput = usemomentum = false;
		momentumfactor = 0.9;
	}

	public void Initialize(int numnodes, NeuralNetworkLayer parent, NeuralNetworkLayer child){

		neuronvalues = new double[numnodes];
		desiredvalues = new double[numnodes];
		errors = new double[numnodes];

		if (parent != null) {
			parentLayer = parent;
		}
	
		if (child != null) {
			childLayer = child;
		
			weights = new double[numnodes][];
			weightchanges = new double[numnodes][];
			for (int i = 0; i<numnodes; i++) {
				weights[i] = new double[childnodes];
				weightchanges[i] = new double[childnodes];
			}
			biasvalues = new double[childnodes];
			biasweights = new double[childnodes];
		} else {
			weights = null;
			biasvalues = null;
			biasweights = null;
		}


		for(int i=0; i<numnodes; i++)
		{
			neuronvalues[i] = 0;
			desiredvalues[i] = 0;
			errors[i] = 0;
			
			if(childLayer != null)
				for(int x=0; x<childnodes; x++)
			{
				weights[i][x] = 0;
				weightchanges[i][x] = 0;
			}
		}

		if(childLayer != null)
			for(int i=0; i<childnodes; i++)
		{
			biasvalues[i] = -1;
			biasweights[i] = 0;
		}
	}

	public void RandomizeWeights()
	{
		int	min = 0;
		int	max = 200;

		for(int i=0; i<nodes; i++)
		{	
			for(int j=0; j<childnodes; j++)
			{   
				weights[i][j] = ((int)Random.Range(min, max)) / 100.0f - 1;
			}
		}

		for(int j=0; j<childnodes; j++)
		{
			biasweights[j] = ((int)Random.Range(min, max)) / 100.0f - 1;
		}
	}

	public void CalculateErrors()
	{
		double sum;

		// output layer
		if(childLayer == null) 
		{
			for(int i=0; i<nodes; i++)
			{
				errors[i] = (desiredvalues[i] - neuronvalues[i]) * neuronvalues[i] * (1.0f - neuronvalues[i]);
			}
		} else if(parentLayer == null) { 
			// input layer
			for(int i=0; i<nodes; i++)
			{
				errors[i] = 0.0f;
			}
		} else {
			// hidden layer
			for(int i=0; i<nodes; i++)
			{
				sum = 0f;
				for(int j=0; j<nodes; j++)
				{
					sum += childLayer.errors[j] * weights[i][j];
				}
				errors[i] = sum * neuronvalues[i] * (1.0f - neuronvalues[i]);
			}
		}
	}

	public void AdjustWeights()
	{
		double dw;
	
		if(childLayer != null)
		{
			for(int i=0; i<nodes; i++)
			{
				for(int j=0; j<childnodes; j++)
				{
					dw = learningrate * childLayer.errors[j] * neuronvalues[i];
					weights[i][j] += dw + momentumfactor * weightchanges[i][j];
					weightchanges[i][j] = dw;
				}
			}
			
			for(int j=0; j<childnodes; j++)
			{
				biasweights[j] += learningrate * childLayer.errors[j] * biasvalues[j];
			}
		}
	}

	public void CalculateNeuronValues()
	{
		double x;
		
		if(parentLayer != null)
		{
			for(int j=0; j<nodes; j++)
			{
				x = 0;
				for(int i=0; i<parentnodes; i++)
				{
					x += parentLayer.neuronvalues[i] * parentLayer.weights[i][j];
				}
				x += parentLayer.biasvalues[j] * parentLayer.biasweights[j];
				
				if((childLayer == null) && linearoutput)
					neuronvalues[j] = x;
				else
					neuronvalues[j] = 1.0f/(1+Mathf.Exp((float)-x));
			}
		}
	}
}
//------Neural Layer Class END-------------------//

