using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class EnemyEyes : MonoBehaviour
{
	//public float updateRate = 0.02f;
	public int quality = 4;
	public int fovAngle = 90;
	public float fovMaxDistance = 15;
	public LayerMask cullingMask;
	public List<RaycastHit2D> hits = new List<RaycastHit2D>();
	
	int numRays;
	float currentAngle;
	Vector3 direction;
	RaycastHit2D hit;
	
	void Update()
	{
		CastRays();
	}
	
	void Start() 
	{
		//InvokeRepeating("CastRays", 0, updateRate);
	}
	
	void CastRays()
	{
		numRays = fovAngle * quality;
		currentAngle = fovAngle / -2;
		
		hits.Clear();
		
		for (int i = 0; i < numRays; i++)
		{
			direction = Quaternion.AngleAxis(currentAngle, transform.up) * transform.forward;

			hit = Physics2D.Raycast (new Vector2 (transform.position.x, transform.position.y), new Vector2 (direction.x, direction.y), fovMaxDistance, cullingMask);

			if(hit.collider == null)
			{
				hit.point = transform.position + (direction * fovMaxDistance);
			}

			hits.Add(hit);


			currentAngle += 1f / quality;
		}
	}
}
