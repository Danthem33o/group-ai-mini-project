﻿using UnityEngine;
using System.Collections;

public class WorkingMemory : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

        public static string strikeA;
        public static string strikeB;
        public static string strikeC;

        public WorkingMemory()
        {
            strikeA = "sUnknown";
            strikeB = "sUnknown";
            strikeC = "sUnknown";
        }

        public static void setStrikeA(string A)
        {
            strikeA = A;
        }

        public static void setStrikeB(string B)
        {
            strikeB = B;
        }

        public static void setStrikeC(string C)
        {
            strikeC = C;
        }

        public static string getStrikeA()
        {
            return strikeA;
        }

        public static string getStrikeB()
        {
            return strikeB;
        }

}
