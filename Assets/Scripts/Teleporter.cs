﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour {

    private PlayerController player;
    public Teleporter teleportPair;
    public bool teleportEnabled;
    public bool multipleTeleports;

    // Use this for initialization
    void Start () {
        player = FindObjectOfType<PlayerController>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter2D(Collider2D character)
    {

        if (character.name == "Player")
        {
            if (teleportEnabled)
            {
                player.transform.position = teleportPair.transform.position;
                if (!multipleTeleports)
                {
                    teleportEnabled = false;
                    teleportPair.teleportEnabled = false;
                }
                   
            }
        }

    }
}

