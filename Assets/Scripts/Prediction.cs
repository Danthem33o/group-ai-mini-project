﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Prediction : MonoBehaviour {

   public TRule[] Rules = new TRule[27];

    private List<string> fightList = new List<string>();
    private List<string> defence = new List<string>();

        public Prediction()
        {
            initialize();
        Debug.Log("initialise");
        }

        public string processMove1(List<string> attack)
        {
        WorkingMemory.setStrikeA(attack[0]);
        int weight = 0;
            string defPrediction = "", prediction = "";
        for (int i = 0; i < 27; i++)
            {
                if (Rules[i].weight >= weight)
                {
                    prediction = Rules[i].antecedentA;
                    weight = Rules[i].weight;
                }
            }
            defPrediction = defend(prediction);
            return defPrediction;
        }

        public string processMove2(List<string> attack)
        {
            WorkingMemory.setStrikeA(attack[0]);
            WorkingMemory.setStrikeB(attack[1]);
            int weight = 0;
            string defPrediction = "", prediction = "";
            for (int i = 0; i < 27; i++)
            {
                if (string.Equals(Rules[i].antecedentA, WorkingMemory.strikeA, StringComparison.OrdinalIgnoreCase) &&
                        Rules[i].weight >= weight)
                {
                    prediction = Rules[i].antecedentB;
                    weight = Rules[i].weight;
                }
            }
            defPrediction = defend(prediction);
            return defPrediction;
        }

        public void initialize()
        {
        defence.Add("Block");
        defence.Add("Parry");
        defence.Add("Dodge");

        for (int i = 0; i < 27; i++)
            {
                TRule Rule = new TRule();
                Rules[i] = Rule;
                Rules[i].weight = GameController.defenseRulesWeight[i];
            }
            Rules[0].setRule("Kick", "Kick", "Kick");
            Rules[1].setRule("Kick", "Kick", "Sword");
            Rules[2].setRule("Kick", "Kick", "Star");
            Rules[3].setRule("Kick", "Sword", "Kick");
            Rules[4].setRule("Kick", "Sword", "Sword");
            Rules[5].setRule("Kick", "Sword", "Star");
            Rules[6].setRule("Kick", "Star", "Kick");
            Rules[7].setRule("Kick", "Star", "Sword");
            Rules[8].setRule("Kick", "Star", "Star");
            Rules[9].setRule("Sword", "Kick", "Kick");
            Rules[10].setRule("Sword", "Kick", "Sword");
            Rules[11].setRule("Sword", "Kick", "Star");
            Rules[12].setRule("Sword", "Sword", "Kick");
            Rules[13].setRule("Sword", "Sword", "Sword");
            Rules[14].setRule("Sword", "Sword", "Star");
            Rules[15].setRule("Sword", "Star", "Kick");
            Rules[16].setRule("Sword", "Star", "Sword");
            Rules[17].setRule("Sword", "Star", "Star");
            Rules[18].setRule("Star", "Kick", "Kick");
            Rules[19].setRule("Star", "Kick", "Sword");
            Rules[20].setRule("Star", "Kick", "Star");
            Rules[21].setRule("Star", "Sword", "Kick");
            Rules[22].setRule("Star", "Sword", "Sword");
            Rules[23].setRule("Star", "Sword", "Star");
            Rules[24].setRule("Star", "Star", "Kick");
            Rules[25].setRule("Star", "Star", "Sword");
            Rules[26].setRule("Star", "Star", "Star");

        }

        public string processMove3(List<string> attack)
        {
            WorkingMemory.setStrikeA(attack[0]);
            WorkingMemory.setStrikeB(attack[1]);
            WorkingMemory.setStrikeC(attack[2]);
            string prediction = "", defPrediction = "";
            int weight, rule = 0;
            for (int i = 0; i < 27; i++)
            {
                if (string.Equals(Rules[i].antecedentA, WorkingMemory.strikeA, StringComparison.OrdinalIgnoreCase) &&
                        string.Equals(Rules[i].antecedentB, WorkingMemory.strikeB, StringComparison.OrdinalIgnoreCase))
                {
                    Rules[i].matched = true;
                }
                else
                {
                    Rules[i].matched = false;
                }

            }
            for (int i = 0; i < 27; i++)
            {
                if (Rules[i].matched == true)
                {
                    weight = Rules[i].weight;
                    prediction = Rules[i].antecedentC;
                    rule = i;
                    for (int j = i + 1; j < 27; j++)
                    {
                        if (Rules[j].matched == true)
                        {
                            if (Rules[j].weight >= weight)
                            {
                                prediction = Rules[j].antecedentC;
                                rule = j;
                            }
                        }
                    }
                    break;
                }
            }
            if (string.Equals(WorkingMemory.strikeC, prediction, StringComparison.OrdinalIgnoreCase))
            {
                Rules[rule].weight++;
            }
            else
            {
                Rules[rule].weight--;
                for (int i = 0; i < 27; i++)
                {
                    if (string.Equals(Rules[i].antecedentA, WorkingMemory.strikeA, StringComparison.OrdinalIgnoreCase) &&
                    string.Equals(Rules[i].antecedentB, WorkingMemory.strikeB, StringComparison.OrdinalIgnoreCase) &&
                    string.Equals(Rules[i].antecedentC, WorkingMemory.strikeC, StringComparison.OrdinalIgnoreCase))
                    {
                        Rules[i].weight++;
                    }
                }
            }
            defPrediction = defend(prediction);
            Console.WriteLine("The system predicted: " + prediction + ", the user entered: " + WorkingMemory.strikeC);
            return defPrediction;
        }

        public string defend(string _attack)
        {
            string _defence = "";
        if (string.Equals(_attack,"kick",StringComparison.OrdinalIgnoreCase))
            {
            _defence = defence[0];
            }
            else if (string.Equals(_attack, "sword", StringComparison.OrdinalIgnoreCase))
            {
                _defence = defence[1];
            }
            else if (string.Equals(_attack, "star", StringComparison.OrdinalIgnoreCase))
            {
                _defence = defence[2];
            }
            return _defence;
        }
}
