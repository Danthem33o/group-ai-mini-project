﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static string battlemode = "MainGame";
    public static int[] attackRulesWeight = new int[27];
    public static int[] defenseRulesWeight = new int[27];
    //set between 1-3;
    public static int difficulty = 1, kills = 0;
    //Cameras for battle and standard stealth scenes.
    public Camera maingame;
    public Camera battle;
    public Canvas turnBasedUI, defenceCanvas, attackCanvas, tttCanvas;
    public TurnBasedController battleManager;
    public GameObject ingameenemy;
    public Canvas HUD;
    public Text pHealth;

    public TTTDoor TTTCodelock;
    private Game TTTGame;
	
    public int enemyid;
    public int enemyHealth;

    private PlayerController player;
    //checkpoint identifier
    public GameObject currentCheckpoint;


    // Use this for initialization.
    void Start (){
        player = FindObjectOfType<PlayerController>();
        battleManager = FindObjectOfType<TurnBasedController>();
        TTTGame = FindObjectOfType<Game>();
        //ensure on start that maingame cam is active.
        maingame.enabled = true;
        battle.enabled = false;
        turnBasedUI.enabled = false;
        defenceCanvas.enabled = false;
        attackCanvas.enabled = false;
        tttCanvas.enabled = false;
        for (int i = 0; i<27; i++)
        {
            attackRulesWeight[i] = 0;
            defenseRulesWeight[i] = 0;
        }
    }
	
	// Update is called once per frame.
	void Update () {
        pHealth.text = "Player Health: " + player.health;
        //switch mode to battlemode
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (battlemode == "TurnBasedGame")
            {
                ActivateMainGame();

            }
            else if (battlemode == "MainGame")
            {
                ActivateTurnBased();
            }
        }
        ManageTTTGame();

    }

    //Display the battlemode to TurnBasedGame
    public void ActivateTurnBased()
    {
            maingame.enabled = false;
            battle.enabled = true;
            battlemode = "TurnBasedGame";
            battleManager.createSprites();
            toggleCanvas();

    }

    public void ActivateTurnBased2(int enemyID, int health, GameObject ingameenemy)
    {
        this.ingameenemy = ingameenemy;
        
        enemyid = enemyID;
        enemyHealth = health;
        maingame.enabled = false;
        battle.enabled = true;
        battlemode = "TurnBasedGame";
        battleManager.createSprites();
        toggleCanvas();

    }

    //Change the battlemode to MainGame
    public void ActivateMainGame()
    {
            maingame.enabled = true;
            battle.enabled = false;
            tttCanvas.enabled = false;
            battlemode = "MainGame";
            toggleCanvas();

    }

    public void ActivateTTT()
    {
        battlemode = "TTT";
        toggleCanvas();

    }

    //Display the required GUI
    void toggleCanvas()
    {
        if (battlemode == "TurnBasedGame")
        {
            DisableAll();
            turnBasedUI.enabled = true;
            
        }
        else if(battlemode == "MainGame")
        {
            DisableAll();
            HUD.enabled = true;
        }
        else if (battlemode == "TTT")
        {
            tttCanvas.enabled = true;
        }
    }

    public void DisableAll()
    {
        Debug.Log("yoooooooooo");
        turnBasedUI.enabled = false;
        defenceCanvas.enabled = false;
        attackCanvas.enabled = false;
        HUD.enabled = false;
    }

    public void RespawnPlayer()
    {
        player.transform.position = currentCheckpoint.transform.position;
        player.grounded = true;
        
    }

    public void ManageTTTGame()
    {
        if (TTTGame.gameOver)
        {
            if (TTTGame.userWon)
                TTTCodelock.DestroyDoorstop();
            else if (TTTGame.draw)
                player.health -= 5;
            else if (TTTGame.aiWon)
                player.health -= 15;
            ActivateMainGame();
            TTTGame.resetBoard();
        }
        if (player.health <= 0)
            RespawnPlayer();
    }

    

}
