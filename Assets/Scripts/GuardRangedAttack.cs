﻿using UnityEngine;
using System.Collections;

public class GuardRangedAttack : MonoBehaviour {
    public int speed;
    private PlayerController player;
    private GameController game;
    Rigidbody2D rb;

	// Use this for initialization
	void Start () {
        player = FindObjectOfType<PlayerController>();
        game = FindObjectOfType<GameController>();
        rb = this.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        rb.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, rb.velocity.y);
	}


    void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.name == "Player")
            player.health -= 15;
        if (player.health <= 0)
        {
            player.lives--;
            player.health = 100;
            game.RespawnPlayer();
            
        }
        if(obj.tag!="Enemy"&&obj.tag!="Ladder")
            Destroy(gameObject);
    }
}
