﻿using UnityEngine;
using System.Collections;

public class KillPlayer : MonoBehaviour {

    private PlayerController playerController;
    private GameController platformer;

	// Use this for initialization
	void Start () {
        playerController = FindObjectOfType<PlayerController>();
        platformer = FindObjectOfType<GameController>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter2D(Collider2D character){
        if(character.name == "Player"){
            if (playerController.lives > 0) {
                playerController.lives -= 1;
                playerController.health = 100;
                platformer.RespawnPlayer();
                    }
            else{
                //game over=> back to main menu
            }
        }

    }
}
