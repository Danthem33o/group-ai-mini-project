﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class GameManager : MonoBehaviour {

    public Text playerHealth;
    public Text enemyHealth, report, special;
    public Character player;
    public Character enemy;
    private GameController gameController;
    public Canvas attackCanvas, defenceCanvas, mainCanvas;
    public Button attackBtn, defenceBtn;
    private List<string> attack;
    private PlayerController playerController;
    private Prediction p;
    private AIAttack a;
    private string combatString;
    public Sprite sprite;

    public List<Sprite> sprites;
    GameObject BattlePlayer;
    GameObject BattleEnemy;
    public Image heart1, heart2, heart3;

    // Use this for initialization
    void Start () {
        playerController = FindObjectOfType<PlayerController>();
        gameController =  this.GetComponent<GameController>();
        p = new Prediction();
        a = new AIAttack();

        attackCanvas = attackCanvas.GetComponent<Canvas>();
        defenceCanvas = defenceCanvas.GetComponent<Canvas>();
        mainCanvas = mainCanvas.GetComponent<Canvas>();
        attackBtn = attackBtn.GetComponent<Button>();
        defenceBtn = defenceBtn.GetComponent<Button>();
        attackCanvas.enabled = false;
        defenceCanvas.enabled = false;
        report.text = "Round 1";
        attackBtn.enabled = true;
        defenceBtn.enabled = false;

        attack = new List<string>();

        //createSprites();

    }

    public void createSprites()
    {
        p = new Prediction();
        a = new AIAttack();

        attackCanvas = attackCanvas.GetComponent<Canvas>();
        defenceCanvas = defenceCanvas.GetComponent<Canvas>();
        mainCanvas = mainCanvas.GetComponent<Canvas>();
        attackBtn = attackBtn.GetComponent<Button>();
        defenceBtn = defenceBtn.GetComponent<Button>();
        attackCanvas.enabled = false;
        defenceCanvas.enabled = false;
        report.text = "Round 1";
        attackBtn.enabled = true;
        defenceBtn.enabled = false;

        attack = new List<string>();
        BattlePlayer = new GameObject("Playa");
        SpriteRenderer renderer = BattlePlayer.AddComponent<SpriteRenderer>();
        renderer.sprite = sprite;
        BattlePlayer.transform.position = new Vector3(-1036, 159, -5);
        BattlePlayer.transform.localScale = new Vector3(2, 2, 1);
        BattleEnemy = new GameObject("Hater");
        SpriteRenderer rendererEnemy = BattleEnemy.AddComponent<SpriteRenderer>();

        rendererEnemy.sprite = sprites[gameController.enemyid];
        BattleEnemy.transform.position = new Vector3(-1030, 159, -5);
        BattleEnemy.transform.localScale = new Vector3(1, 1, 1);
        //gets the scale of the transform
        Vector3 scale = BattleEnemy.transform.localScale;
        //changes all x values to their negative counterpart
        scale.x *= -1;
        //asigns scale to game
        BattleEnemy.transform.localScale = scale;
        BattlePlayer.AddComponent<Character>();
        BattleEnemy.AddComponent<Character>();
        player = BattlePlayer.GetComponent<Character>();
        enemy = BattleEnemy.GetComponent<Character>();
        player.SetArgs(playerController.health, 0);
        enemy.SetArgs(gameController.enemyHealth, 0);
        //enemy.SetArgs(enemyController.health,0);
        playerHealth.text = "Player health: " + player.health.ToString();
        enemyHealth.text = "Enemy health: " + enemy.health.ToString();// enemy.health.ToString();


    }
    
    public void Attack()
    {if (GameController.battlemode == "TurnBasedGame")
        {
            attackCanvas.enabled = true;
            report.text = "Attack 1: ";
            defenceCanvas.enabled = false;
        }
    }

    public void Defend()
    {
        if (GameController.battlemode == "TurnBasedGame")
        {
            defenceCanvas.enabled = true;
            attackCanvas.enabled = false;
        }
    }

    public void Kick()
    {
        attack.Add("Kick");
        ProcessMoves();
    }

    public void Sword()
    {
        attack.Add("Sword");
        ProcessMoves();
    }

    public void Bow()
    {
        attack.Add("Bow");
        ProcessMoves();
    }

    public void Block()
    {
        attack.Add("Block");
        ProcessDefenceMoves();
    }

    public void Parry()
    {
        attack.Add("Parry");
        ProcessDefenceMoves();
    }

    public void Strife()
    {
        attack.Add("Strife");
        ProcessDefenceMoves();
    }

    public void ProcessMoves()
    {if (GameController.battlemode == "TurnBasedGame")
        {
            if (attack.Count == 1)
            {
                string defence = p.processMove1(attack);
                string combatStatus = Combat.processStrikeA(player, attack[0], enemy, defence);
                report.text = defence;
                special.text = combatStatus;
                updateCharacters();
            }
            else if (attack.Count == 2)
            {
                string defence = p.processMove2(attack);
                string combatStatus = Combat.processStrikeA(player, attack[1], enemy, defence);
                report.text = defence;
                special.text = combatStatus;
                updateCharacters();
            }
            else if (attack.Count == 3)
            {
                string defence = p.processMove3(attack);
                string combatStatus = Combat.processStrikeA(player, attack[2], enemy, defence);
                report.text = defence;
                attack.Clear();
                special.text = combatStatus;
                updateCharacters();
                attackCanvas.enabled = false;
                attackBtn.enabled = false;
                defenceBtn.enabled = true;
            }
        }
    }

    public void ProcessDefenceMoves()
    {
        if (GameController.battlemode == "TurnBasedGame")
        {
            string AIAttack = "";
            if (attack.Count == 1)
            {
                AIAttack = a.processMove1(attack);
                String combatStatus = Combat.processStrikeA(enemy, AIAttack, player, attack[0]);
                special.text = combatStatus;
                updateCharacters();
            }
            else if (attack.Count == 2)
            {
                AIAttack = a.processMove2(attack);
                String combatStatus = Combat.processStrikeA(enemy, AIAttack, player, attack[1]);
                special.text = combatStatus;
                updateCharacters();
            }
            else if (attack.Count == 3)
            {
                AIAttack = a.processMove3(attack);
                String combatStatus = Combat.processStrikeA(enemy, AIAttack, player, attack[2]);
                attack.Clear();
                special.text = combatStatus;
                updateCharacters();
                defenceCanvas.enabled = false;
                attackBtn.enabled = true;
                defenceBtn.enabled = false;
            }
            report.text = AIAttack;
        }
    }

    public void updateCharacters()
    {
        playerController.health = player.health;
        playerHealth.text = "Player health: " + playerController.health.ToString();
        enemyHealth.text = "Enemy health: " + enemy.health.ToString();
    }

    // Update is called once per frame
    void Update() {
        if(playerController.lives == 1) {
            heart3.enabled = false;
            heart2.enabled = false;
        } else if (playerController.lives == 2)
        {
            heart3.enabled = false;
        }
        {
            if (playerController.health <= 0 && playerController.lives > 0)
            {
                playerController.lives -= 1;
                SaveWeights();
                ManageModeSwitch();
                
            }
            else if (playerController.lives < 0)
            {
                //TO be replaced with GAME OVER MOTHER FUCKA
                playerController.lives = 3;
                heart1.enabled = true;
                heart3.enabled = true;
                heart2.enabled = true;
                SaveWeights();
                ManageModeSwitch();
            }
            else if (enemy.health <= 0)
            {
                Destroy(gameController.ingameenemy.gameObject);
                Destroy(BattlePlayer.gameObject);
                Destroy(BattleEnemy.gameObject);
                SaveWeights();
                gameController.ActivateMainGame();
            }
        }
    }

    void ManageModeSwitch()
    {
        gameController.RespawnPlayer();
        playerController.health = 100;
        Destroy(BattlePlayer.gameObject);
        Destroy(BattleEnemy.gameObject);
        gameController.ActivateMainGame();
    }

    void SaveWeights()
    {
        for(int i = 0; i < 27; i++)
        {
            GameController.attackRulesWeight[i] = a.Rules[i].weight;
            GameController.defenseRulesWeight[i] = p.Rules[i].weight;
        }
    }


}

//add gamecontroller