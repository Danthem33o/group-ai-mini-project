﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class AIAttack : MonoBehaviour {

    public AIAttack()
    {
        initialize();
    }

    public TRule[] Rules = new TRule[27];

        List<String> fightList = new List<String>();
        List<String> attacks = new List<String>();


        public String processMove1(List<string> defend)
        {
            WorkingMemory.setStrikeA(defend[0]);
            int weight = 0;
            String defPrediction = "", prediction = "";
            for (int i = 0; i < 27; i++)
            {
                if (Rules[i].weight >= weight)
                {
                    prediction = Rules[i].antecedentA;
                    weight = Rules[i].weight;
                }
            }
            defPrediction = attack(prediction);
            return defPrediction;
        }

        public String processMove2(List<string> defend)
        {
            WorkingMemory.setStrikeA(defend[0]);
            WorkingMemory.setStrikeB(defend[1]);
            int weight = 0;
            String defPrediction = "", prediction = "";
            for (int i = 0; i < 27; i++)
            {
                if (string.Equals(Rules[i].antecedentA, WorkingMemory.strikeA, StringComparison.OrdinalIgnoreCase) &&
                        Rules[i].weight >= weight)
                {
                    prediction = Rules[i].antecedentB;
                    weight = Rules[i].weight;
                }
            }
            defPrediction = attack(prediction);
            return defPrediction;
        }

        public void initialize()
        {
            attacks.Add("Kick");
        attacks.Add("Sword");
        attacks.Add("Star");
        for (int i = 0; i < 27; i++)
        {
            TRule Rule = new TRule();
            Rules[i] = Rule;
            Rules[i].weight = GameController.attackRulesWeight[i];
        }
            //System.out.println(Rules[0].weight);
            Rules[0].setRule("Block", "Block", "Block");
            Rules[1].setRule("Block", "Block", "Parry");
            Rules[2].setRule("Block", "Block", "Dodge");
            Rules[3].setRule("Block", "Parry", "Block");
            Rules[4].setRule("Block", "Parry", "Parry");
            Rules[5].setRule("Block", "Parry", "Dodge");
            Rules[6].setRule("Block", "Dodge", "Block");
            Rules[7].setRule("Block", "Dodge", "Parry");
            Rules[8].setRule("Block", "Dodge", "Dodge");
            Rules[9].setRule("Parry", "Block", "Block");
            Rules[10].setRule("Parry", "Block", "Parry");
            Rules[11].setRule("Parry", "Block", "Dodge");
            Rules[12].setRule("Parry", "Parry", "Block");
            Rules[13].setRule("Parry", "Parry", "Parry");
            Rules[14].setRule("Parry", "Parry", "Dodge");
            Rules[15].setRule("Parry", "Dodge", "Block");
            Rules[16].setRule("Parry", "Dodge", "Parry");
            Rules[17].setRule("Parry", "Dodge", "Dodge");
            Rules[18].setRule("Dodge", "Block", "Block");
            Rules[19].setRule("Dodge", "Block", "Parry");
            Rules[20].setRule("Dodge", "Block", "Dodge");
            Rules[21].setRule("Dodge", "Parry", "Block");
            Rules[22].setRule("Dodge", "Parry", "Dodge");
            Rules[23].setRule("Dodge", "Parry", "Dodge");
            Rules[24].setRule("Dodge", "Dodge", "Block");
            Rules[25].setRule("Dodge", "Dodge", "Parry");
            Rules[26].setRule("Dodge", "Dodge", "Dodge");

        }

        public String processMove3(List<string> defend)
        {
            WorkingMemory.setStrikeA(defend[0]);
            WorkingMemory.setStrikeB(defend[1]);
            WorkingMemory.setStrikeC(defend[2]);
            string defPrediction = "", prediction = "";
            int weight, rule = 0;
            for (int i = 0; i < 27; i++)
            {
                if (string.Equals(Rules[i].antecedentA, WorkingMemory.strikeA, StringComparison.OrdinalIgnoreCase) &&
                        string.Equals(Rules[i].antecedentB, WorkingMemory.strikeB, StringComparison.OrdinalIgnoreCase))
                {
                    Rules[i].matched = true;
                }
                else
                {
                    Rules[i].matched = false;
                }

            }
            for (int i = 0; i < 27; i++)
            {
                if (Rules[i].matched == true)
                {
                    weight = Rules[i].weight;
                    prediction = Rules[i].antecedentC;
                    rule = i;
                    for (int j = i + 1; j < 27; j++)
                    {
                        if (Rules[j].matched == true)
                        {
                            if (Rules[j].weight >= weight)
                            {
                                prediction = Rules[j].antecedentC;
                                rule = j;
                            }
                        }
                    }
                    break;
                }
            }
            if (string.Equals(WorkingMemory.strikeC, prediction, StringComparison.OrdinalIgnoreCase))
            {
                Rules[rule].weight++;
            }
            else
            {
                Rules[rule].weight--;
                for (int i = 0; i < 27; i++)
                {
                    if (string.Equals(Rules[i].antecedentA, WorkingMemory.strikeA, StringComparison.OrdinalIgnoreCase) &&
                            string.Equals(Rules[i].antecedentB, WorkingMemory.strikeB, StringComparison.OrdinalIgnoreCase)
                            && string.Equals(Rules[i].antecedentC, WorkingMemory.strikeC, StringComparison.OrdinalIgnoreCase))
                    {
                        Rules[i].weight++;
                    }
                }
            }

            defPrediction = attack(prediction);
            Console.WriteLine("The system predicted: " + prediction + ", the user entered: " + WorkingMemory.strikeC);
        Console.WriteLine(defPrediction);
            return defPrediction;
        }

        public string attack(string _defend)
        {
            string _attack = "";
            for (int i = 0; i < 3; i++)
            {
                if (string.Equals(_defend, "block", StringComparison.OrdinalIgnoreCase))
                {
                    _attack = attacks[1];
                }
                else if (string.Equals(_defend, "parry", StringComparison.OrdinalIgnoreCase))
                {
                    _attack = attacks[2];
                }
                else if (string.Equals(_defend, "dodge", StringComparison.OrdinalIgnoreCase))
                {
                    _attack = attacks[0];
                }
            }
            return _attack;
        }
}
